# NESlang (working title)

NESlang aims to be a domain specific language to develop [NES (Nintendo
Entertainment
System)](https://fr.wikipedia.org/wiki/Nintendo_Entertainment_System) games
easily without having to worry about the hardware specificities of the console.

## Overview

This projects has the following goals:

- provide a language that is simple to use, and designed around game development;
- compile to performant and lightweight 6502 assembly code (because of the NES'
  hardware restrictions);
- handle all quirks of NES game development transparently, such as managing
  the PPU, the APU, etc.

Eventually, NESlang could also aim to provide the following:

- a platform for users to publish and download libraries to further ease game
  development, and make it a community effort;
- allow the most modularity possible by allowing users to write assembly themselves
  and provide hight level abstractions, as well as low level control over the
  hardware (which is needed for these kind of old consoles);
- provide debugging tools.

The project is written in Rust and will be published under an open source
copyleft licence (though the exact license hasn't been chosen yet).
